import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit] : ");
        number = sc.nextInt();

        int n;

        if (number == 1) {
            System.out.print("Please input number : ");
            n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                for (int y = 0; y < i + 1; y++) {
                    System.out.print("*");
                }
                System.out.println("");
            }
        } else if (number == 2) {
            System.out.print("Please input number : ");
            n = sc.nextInt();
            for (int i = n; i > 0; i = i - 1) {
                for (int y = 0; y < i; y++) {
                    System.out.print("*");
                }
                System.out.println("");
            }
        } else if (number == 3) {
            System.out.print("Please input number : ");
            n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                for (int y = 0; y < i; y++) {
                    System.out.print(" ");
                }
                for (int y = 0; y < (n - i); y++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (number == 4) {
            System.out.print("Please input number : ");
            n = sc.nextInt();
            for (int i = 4; i >= 0; i--) {
                for (int y = 0; y < n; y++) {
                    if (y >= i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        } else if (number == 5) {
            System.out.println("Bye bye!!!");
        } else {
            System.out.println("Error: Please input number between 1-5");
        }

    }
}
