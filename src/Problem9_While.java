import java.util.Scanner;

public class Problem9_While {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n : ");
        n = sc.nextInt();

        int i = 1;
        while (i < n+1) {
            int y = 1;
            while (y < n+1) {
                System.out.print(y);
                y++;
            }
            System.out.println();
            i++;
        }
    }
}
