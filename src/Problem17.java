public class Problem17 {
    public static void main(String[] args) {
        for (int i = 5; i >= 1; i--) {
            for(int y = 0; y <= 5; y++) {
                if(y >= i) {
                    System.out.print(y);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
