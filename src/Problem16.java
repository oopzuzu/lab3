public class Problem16 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int y = 1; y < i; y++) {
                System.out.print(" ");
            }
            for (int y = i; y <= 5; y++) {
                System.out.print(y);
            }
            System.out.println();
        }
    }
}
