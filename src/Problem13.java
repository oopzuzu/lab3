public class Problem13 {
    public static void main(String[] args) {
        for (int i = 4; i >= 0; i--) {
            for(int y = 0; y < 5; y++) {
                if(y >= i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
