public class Problem8_While {
    public static void main(String[] args) {
        int i = 1;
        while (i < 6) {
            int y = 1;
            while (y < 6) {
                System.out.print(y);
                y++;
            }
            System.out.println();
            i++;
        }
    }
}
