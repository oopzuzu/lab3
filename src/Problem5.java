import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String str1;
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.print("Please input : ");
            str1 = sc.nextLine();
            if (str1.trim().equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                break;

            } 
            System.out.println(str1);
        }
        sc.close();
    }
}
