public class Problem6_While {
    public static void main(String[] args) {
        int i = 0 ;
        while (i < 5) {
            int y = 0 ;
            while (y < 5){
                System.out.print("*");
                y++;
            }
            System.out.println();
            i++;
        }
    }
}
