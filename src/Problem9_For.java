import java.util.Scanner;

public class Problem9_For {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n : ");
        n = sc.nextInt();

        for (int i = 1; i < (n+1); i++) {
            for (int y = 1; y < (n+1); y++) {
                System.out.print(y);
            }
            System.out.println();
        }
    }
}
